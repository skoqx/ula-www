# ula-www

## Strona internetowa - wymagania
Tworząc stronę internetową, trzeba ją tworzyć w standardzie HTML5 z jednym arkuszem styli w osobnym pliku, zgodnie z zasadami poradnika Damiana Wielgosika

(Żadnych niewidocznych tabelek do tworzenia układów stron!!! Żadnych znaczników FONT czy CENTER w kodzie strony)
Do formatowania strony służą pliki css. Pamiętamy o dwóch sposobach centrowania:

```
"text-align: center;" - dla tekstu
"margin: 0 auto;" - dla pudełek <figure> ... </figure>
```

Strona może być zrobiona na bazie gotowego szablonu, ale koniecznie musi być to szablon zgodny z standardem HTML5. Koniecznie musimy zachować informacje świadczące o autorstwie szablonu w stopce strony i oczywiście nie wybieramy szablonów których nie rozumiemy. Sprawdzam czy osoba oddająca rozumie wszystkie elementy budowy szablonu!!! (również css)

Strona powinna składać się z trzech podstron:

- strona z dwoma artykułami. Artykuł powinien mieć jakąś grafikę i ok 50 słów. Powinien być waszego autorstwa. Strona musi mieć trzy kolumny, nagłówek i stopkę.

- strona Twojego autorstwa z auto prezentacją, galerią zdjęć oraz wypowiedzią na poważny temat który Cię zajmuje intelektualnie. Strony należy nazywać używając nazwy: o_ani.html, str_andrzeja.html lub podobnie. Nie stosujemy nazw s1.html . 

- strona startowa koniecznie musi nazywać się index.html i posiadać dołączony jeden arkusz styli style.css (może być inna nazwa). Obrazy które zdobią stronę powinny znajdować się w osobnym folderze, który znajduje się obok. Proponowana nazwa to np.: images lub obrazy. Obrazy na stronie muszą być ciekawie rozmieszczone względem innych obrazów lub tekstu. Niedopuszczalna jest strona w całości wyśrodkowana lub wszystko przy brzegu.

Strona powinna mieć stonowane i zharmonizowane kolory. (można je pobrać ze stron które nam się podobają)
Tekst nie powinien przylegać do brzegu (mamy atrybut pudełka - padding)

Konieczne jest zastosowanie kolumn na stronie z użyciem pojemników ustawionych obok siebie - omawiałam to na lekcji. Strony nie spełniające wymagań będą oceniane na ocenę mniejsza niż 4. Brak pracy to ocena ndst. Prace bardziej kreatywne niż przerobienie szablonu 5 a na ocenę 6 trzeba odpowiedzieć ustnie nt. budowy swojej strony.